-- Primeiro dia do Advento de código
import Data.List(nub)

cortador :: [String] -> [[String]]
cortador [] = []
cortador c  = case dropWhile cortaVazio c of 
    []        -> [takeWhile cortaVazio c]
    otherwise -> [takeWhile cortaVazio c] ++ cortador (tail (dropWhile cortaVazio c))
    where cortaVazio :: String -> Bool
          cortaVazio cr = cr /= ""

transformador :: [[String]] -> [[Int]]
transformador [] = []
transformador c  = (map . map) (read :: String -> Int) c

contador :: [[Int]] -> [Int]
contador i = map sum i

---------------

removedor :: Eq a => a -> [a] -> [a]
removedor i []     = []
removedor i (n:ns) = if n == i then ns
                       else [n] ++ removedor i ns


troncal :: IO()
troncal = do
    texto       <- readFile "input.txt"
    let cortado  = contador $ transformador $ cortador $ lines texto
    let sol      = maximum cortado
    putStrLn $ show cortado
    putStrLn $ "O duende que tem mais calorias tem: " ++ show sol
    putStrLn $ show $ length cortado
    let un = (length cortado) == (length (nub cortado))
    putStrLn $ show un
    let segundo = maximum (removedor sol cortado)
    let trio = [sol, segundo] ++ [maximum (removedor segundo (removedor sol cortado))]
    putStrLn $ "Os três duendes têm " ++ show trio ++ " e em total têm " ++ show (sum trio)